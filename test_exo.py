import pytest

def division(x, y):
    return x / y

@pytest.mark.divided
def test_division_1():
    assert division(8, 2) == 4


@pytest.mark.divided
def test_division_2():
    assert division(20, 10) == 2

@pytest.mark.divided
def test_division_2():
    assert division(10, 2) == 5


def test_division_type():
    assert isinstance(division(10, 2),float)